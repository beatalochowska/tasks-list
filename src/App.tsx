import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core'
import blue from '@material-ui/core/colors/blue';
import Table from './components/Table';

const App = () => {
  const theme = createMuiTheme({
    palette: {
      primary: blue,
      secondary: {
        main: '#f44336',
      },
    },
  });
  return (
    <MuiThemeProvider theme={theme} >
      <div style={{ margin: "8px" }}>
        <Table />
      </div>

    </MuiThemeProvider >
  );
}

export default App;
