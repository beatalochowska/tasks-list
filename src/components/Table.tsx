import React, { useEffect, useState } from 'react'
import MaterialTable from 'material-table'
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { createMuiTheme } from '@material-ui/core/styles';
import { Tooltip } from '@material-ui/core';
import { fetchFakeData } from '../api/fakeApi';
import { fakeData } from "../api/tableData";

import failure from '../images/failure.png';
import success from '../images/success.png';
import unstable from '../images/unstable.png';
import waiting from '../images/waiting.png';

import charged0 from '../images/charged0.png';
import charged25 from '../images/charged25.png';
import charged50 from '../images/charged50.png';
import charged75 from '../images/charged75.png';
import charged99 from '../images/charged99.png';
import charged100 from '../images/charged100.png';

const Table = () => {
  const [rows, setRows] = useState([]);
  const [isLoading, handleIsLoading] = useState(false);
  const [isLoaded, handleIsLoaded] = useState(false);
  const [isError, handleIsError] = useState(false);
  const [errorMessage, handleErrorMessage] = useState("");
  const [data] = useState(JSON.parse(fakeData));

  interface Data {
    status: number;
    done: number;
    author: string;
    comaker: string;
    task: string;
    comments: string;
    statusIcon: string;
    statusDescription: string;
    healthIcon: string
  }

  data.map((el: Data) => {
    if (el["status"] === 13) {
      el["statusIcon"] = failure
      el["statusDescription"] = "failure"
    }
    else if (el["status"] === 12) {
      el["statusIcon"] = success
      el["statusDescription"] = "success"
    }
    else if (el["status"] === 11) {
      el["statusIcon"] = unstable
      el["statusDescription"] = "unstable"
    }
    else if (el["status"] === 14) {
      el["statusIcon"] = waiting
      el["statusDescription"] = "waiting"
    }
  }
  )

  data.map((el: Data) => {
    if (el["done"] === 0) {
      el["healthIcon"] = charged0
    }
    else if ( el["done"] <= 25) {
      el["healthIcon"] = charged25
    }
    else if (el["done"] <= 50) {
      el["healthIcon"] = charged50
    }
    else if (el["done"] <= 75) {
      el["healthIcon"] = charged75
    }
    else if (el["done"] <= 99) {
      el["healthIcon"] = charged99
    }
    else if (el["done"] === 100) {
      el["healthIcon"] = charged100
    }
  })

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#2196f3',
      },
      secondary: {
        main: '#3f51b5',
      },
    },
    overrides: {
      MuiTableCell: {
        root: {
          paddingRight: 10,
          paddingLeft: 10
        }
      }
    }
  });

  useEffect(() => {
    handleIsLoading(true)
    fetchFakeData()
      .then((fakeData) => {
        setRows(JSON.parse(fakeData));
        handleIsLoading(false)
        handleIsLoaded(true)
      })
      .catch(err => {
        handleIsError(true)
        handleErrorMessage(err)
      })
      .finally(() => {
        handleIsLoading(false)
      })
  }, [])

  if (isLoading) {
    return (
      <div>is loading</div>
    );
  }

  if (isError) {
    return (
      <div>Error!!! {errorMessage}</div>
    )
  }

  if (isLoaded && rows.length === 0) {
    return <div>pusta tabela, brak danych</div>
  }

  if (isLoaded && rows.length > 0) {
    return (
      <MuiThemeProvider theme={theme}>
        <MaterialTable
          columns={[
            {
              filterCellStyle: {
                width: "3vw",
              },
              title: 'Status',
              field: 'status',
              render: rowData => <Tooltip title={rowData.statusDescription} placement="right">
                <img src={rowData.statusIcon} style={{ width: 20, borderRadius: '50%', marginLeft: 17 }} />
              </Tooltip>,
              lookup: { 11: 'unstable', 12: 'success', 13: 'failure', 14: 'waiting' },
            },
            {
              filterCellStyle: {
                width: "5vw",
              },
              title: 'Done',
              render: rowData => <Tooltip title={rowData.done + '%'} placement="right">
                <img src={rowData.healthIcon} style={{ width: 15, marginRight: 30 }} />
              </Tooltip>,
              field: 'done',
              type: 'numeric',
            },
            {
              filterCellStyle: {
                width: "10vw",
              },
              title: 'Author',
              field: 'author'
            },
            { 
              filterCellStyle: {
                width: "10vw",
              },
              title: 'Comaker', 
              field: 'comaker' 
            },
            { title: 'Task', field: 'task' },
            { title: 'Comments', field: 'comments' },
          ]}

          data={data}
          options={{
            filtering: true,
            search: false,
            sorting: true,
          }}

          components={{
            Toolbar: props => null
          }}
        />
      </MuiThemeProvider>
    )
  }
  return null;
}

export default Table;
