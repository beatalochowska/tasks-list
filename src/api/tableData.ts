export const fakeData = `[
    {
        "status": 12,
        "done": 90,
        "author": "Alexandra",
        "comaker": "Deborah",
        "task": "UX research",
        "comments": "almost finished" 
    },
    {
        "status": 13,
        "done": 20,
        "author": "Wanda",
        "comaker": "Natalie",
        "task": "Update front-end for web app. Adding new features.",
        "comments": "communication problems"
    },
    {
        "status": 11,
        "done": 80,
        "author": "Elisabeth",
        "comaker": "Joseph",
        "task": "Creating graphics for web app. Change color palette. Work close with front-end team.",
        "comments": "waiting for the content"
    },
    {
        "status": 14,
        "done": 30,
        "author": "Celine",
        "comaker": "Tom",
        "task": "Update back-end for web app. Adding new features.",
        "comments": "waiting for code review"
    },
    {
        "status": 11,
        "done": 0,
        "author": "Pedro",
        "comaker": "Bernard",
        "task": "Tests",
        "comments": "not started"
    },
    {
        "status": 12,
        "done": 88,
        "author": "Piter",
        "comaker": "Georg",
        "task": "Preparing team integration",
        "comments": "no comments"
    },
    {
        "status": 12,
        "done": 100,
        "author": "Henry",
        "comaker": "Leo",
        "task": "Planning",
        "comments": "finished"
    }
   
]
`;
